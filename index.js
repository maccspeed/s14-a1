console.log("Hello World!");

let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
	city: "Lincoln",
	houseNumber: "32",
	state: "Nebraska",
	street: "Washington",
}

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies: ", hobbies);
console.log("Work Address: ", workAddress);

function myFunc(firstName, lastName, age) {
	console.log(firstName, lastName, "is " + age + " years of age.");
	console.log("This was printed inside of the function", hobbies);
	console.log("This was printed inside of the function", workAddress);
}

function myReturnFunc() {
	let isMarried = true;
	return isMarried;
}

myFunc(firstName, lastName, age);
result = myReturnFunc();

console.log("The value of isMarried:", result);